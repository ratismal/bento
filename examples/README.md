# Bento Examples

### How do I run an example?

Install and build Bento Examples:
```sh
npm install
npm run build
# or
yarn install
yarn run build
```

Determine the example you want to run (ex: `bento-variables`) then execute the following
```sh
node build/{exampleName}

# ex: if I wanted to run bento-variables
node build/bento-variables
```
