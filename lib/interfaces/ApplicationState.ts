'use strict';

export interface ApplicationState {
	components: Array<string>;
	plugins: Array<string>;
	variables: Array<string>;
}
