'use strict';

export enum SubscriptionType {
	EVENT = 0,
	SUBJECT = 1,
}
